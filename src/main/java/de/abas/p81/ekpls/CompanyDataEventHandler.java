package de.abas.p81.ekpls;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.abas.erp.api.gui.TextBox;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.ButtonEventHandler;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.axi2.type.ButtonEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.company.CompanyDataEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;

@EventHandler(head = CompanyDataEditor.class)

@RunFopWith(EventHandlerRunner.class)

public class CompanyDataEventHandler {

	@ButtonEventHandler(field="yekplstestmail", type = ButtonEventType.AFTER)
	public void yekplstestmailAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CompanyDataEditor head) throws EventException {
		if(hasTestableConditions(head)){
			try{
				sendEmailFromSmtp(ctx, head);
			}catch(Exception e){
				ctx.out().print(e.getMessage());
			}
		}
		
		
	}


	private void sendEmailFromSmtp(DbContext ctx, CompanyDataEditor head) throws Exception {
		String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
		EmailHandler.senderEmail = head.getYekplsusername();
		EmailHandler.senderPassword = head.getYekplspassword();
		EmailHandler.host = head.getYekplshost();
		EmailHandler.port = head.getYekplsport();
		EmailHandler.hasAuth = true;
		EmailHandler.enabletls = true;
		EmailHandler.sendAsHtml(head.getYekplstestmailrec(),
		          "Test email",
		          "<h2>Email from abas</h2><p>hi there! it works...</p><p>abas Server date/time : "+timeStamp+"</p>");
		new TextBox(ctx, "Send Email Status", "Email sent successfully.").show();
	}
	
	
	private boolean hasTestableConditions(CompanyDataEditor head){
		return true;
	}

}
