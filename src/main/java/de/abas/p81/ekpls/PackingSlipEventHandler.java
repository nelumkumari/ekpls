package de.abas.p81.ekpls;



import de.abas.erp.api.gui.ButtonSet;
import de.abas.erp.api.gui.TextBox;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.common.type.IdImpl;
import de.abas.erp.common.type.enums.EnumDialogBox;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.exception.CommandException;
import de.abas.erp.db.schema.company.CompanyData;
import de.abas.erp.db.schema.purchasing.Item;
import de.abas.erp.db.schema.purchasing.PackingSlipEditor;
import de.abas.erp.db.schema.purchasing.PackingSlipEditor.Row;
import de.abas.erp.db.schema.purchasing.PurchaseOrder;
import de.abas.erp.db.schema.purchasing.PurchaseOrderEditor;
import de.abas.erp.db.schema.vendor.SelectableVendor;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.db.selection.ExpertSelection;
import de.abas.erp.db.selection.Selection;
import de.abas.erp.db.util.QueryUtil;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.abas.erp.axi2.event.ScreenEndEvent;

@EventHandler(head = PackingSlipEditor.class, row = PackingSlipEditor.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class PackingSlipEventHandler {
	

	
	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, PackingSlipEditor head)
			throws EventException {
		if (head.getPostPackSlipInvoice()) {
			boolean isOverDelivery = false;
			boolean isUnderDelivery = false;
			String messageBody = "<table border='1' width='700'><tr><td>Product No</td><td>Searchword</td><td>Purchase Qty</td><td>Delivery Qty</td></tr>";
			// Set<String> underDeliveryPOItems = new LinkedHashSet<String>();
			String underDeliveryPONumbers = "";
			Vendor vendor = getVendorObjectById(ctx, head.getVendor());
			double upperLimit = vendor.getYekplsover().doubleValue();
			double lowerLimit = vendor.getYekplsunder().doubleValue();

			Iterable<Row> packingSlipRows = head.table().getEditableRows();
			for (Row packingSlipRow : packingSlipRows) {
				double purchasedQty = packingSlipRow.getSalesPurchQty().doubleValue();
				double deleveryQty = packingSlipRow.getUnitQty().doubleValue();
				double qtyDifference = purchasedQty - deleveryQty;
				double diferencePercentage = 100 - ((qtyDifference / purchasedQty) * 100);
				packingSlipRow.setYekplspoitemid("");
				if (qtyDifference > 0) {
					if (diferencePercentage < lowerLimit) {

						msg(ctx, diferencePercentage, upperLimit, lowerLimit, "below the under limit !!!");
						isUnderDelivery = true;
						// underDeliveryPOItems.add(packingSlipRow.getOrigItem().getId().toString());
						packingSlipRow.setYekplspoitemid(packingSlipRow.getOrigItem().getId().toString());
						if(!underDeliveryPONumbers.contains(packingSlipRow.getOrigItemHead().getIdno())){
							underDeliveryPONumbers += packingSlipRow.getOrigItemHead().getIdno() + ",";
						}
						
					} else {
						msg(ctx, diferencePercentage, upperLimit, lowerLimit, "safe to go with under delivery");
					}
				} else if (qtyDifference < 0) {
					if (diferencePercentage > upperLimit) {
						msg(ctx, diferencePercentage, upperLimit, lowerLimit, "over the over limit !!!");
						isOverDelivery = true;
						messageBody += "<tr>"
										+ "<td>"+packingSlipRow.getProduct().getIdno()+"</td>"
										+ "<td>"+packingSlipRow.getProduct().getSwd()+"</td>"
										+ "<td>"+packingSlipRow.getSalesPurchQty()+"</td>"
										+ "<td>"+packingSlipRow.getUnitQty()+"</td>"
										+ "</tr>";
					} else {
						msg(ctx, diferencePercentage, upperLimit, lowerLimit, "safe to go with over delivery");
					}
				} else {

				}

			}

			if (isOverDelivery && !isUnderDelivery) {
				handleOverDelivery(ctx, messageBody, head);
			} else if (!isOverDelivery && isUnderDelivery) {
				head.setYekplsunderpono(underDeliveryPONumbers);
				handleUnderDelivery(ctx,head);
				// handle at exit screen
			} else if (isOverDelivery && isUnderDelivery) {
				boolean flag = handleOverDelivery(ctx, messageBody, head);
				if(flag){
					head.setYekplsunderpono(underDeliveryPONumbers);
					handleUnderDelivery(ctx,head);
				}
			}

		}

	}

	private boolean handleOverDelivery(DbContext ctx, String messageBody, PackingSlipEditor head) {
		TextBox textBox = new TextBox(ctx, "Over Delivery !!!", "Are you sure to continue an over delivery?");
		textBox.setButtons(ButtonSet.NO_YES);
		EnumDialogBox boxResult = textBox.show();
		if (boxResult.equals(EnumDialogBox.No)) {
			head.setPostPackSlipInvoice(false);
			try {
				sendEmailFromSmtp(ctx, "Over delivery (Packingslip no : " + head.getIdno() + ")",
						"<h2>Over delivery : " + head.getIdno() + " </h2><p>" + messageBody + "</p>");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}
		return true;
	}

	 private void handleUnderDelivery(DbContext ctx,PackingSlipEditor head) {
		 TextBox textBox = new TextBox(ctx,"Under Delivery !!!","Do you want to close the remaining amounts from purchasing order(s) " + 	 head.getYekplsunderpono() + "?");
		 textBox.setButtons(ButtonSet.NO_YES);
		 EnumDialogBox boxResult = textBox.show();
		 if(!boxResult.equals(EnumDialogBox.Yes)){
			 head.setYekplsunderpono("");
		 }
	 }
	

	public Vendor getVendorObjectById(DbContext ctx, SelectableVendor selectableVendor) {
		return ctx.load(Vendor.class, new IdImpl(selectableVendor.getId().toString()));
	}

	public Item getPurchaseOrderRow(DbContext ctx, String purchaseOrderRow) {
		return ctx.load(Item.class, new IdImpl(purchaseOrderRow));
	}

	public void msg(DbContext ctx, double diferencePercentage, double upperLimit, double lowerLimit, String text) {
		// ctx.out().print(text + "\ndiferencePercentage=" + diferencePercentage
		// + "\nupperLimit=" + upperLimit + "\nlowerLimit=" + lowerLimit );
	}

	private void sendEmailFromSmtp(DbContext ctx, String subject, String body) throws Exception {
		String criteria = "idno==1;@filingmode=(Active)";
		Selection<CompanyData> selection = ExpertSelection.create(CompanyData.class, criteria);
		CompanyData companyData = QueryUtil.getFirst(ctx, selection);
		EmailHandler.senderEmail = companyData.getYekplsusername();
		EmailHandler.senderPassword = companyData.getYekplspassword();
		EmailHandler.host = companyData.getYekplshost();
		EmailHandler.port = companyData.getYekplsport();
		EmailHandler.hasAuth = true;
		EmailHandler.enabletls = true;
		//ctx.out().print("SDFSDFSDFSD");
		String recepients = companyData.getYekplsrecemail1() + "," + companyData.getYekplsrecemail2() + "," + companyData.getYekplsrecemail3();
		EmailHandler.sendAsHtml(recepients, subject, body);
		new TextBox(ctx, "Send Email Status", "Email sent successfully.").show();
	}





	@ScreenEventHandler(type = ScreenEventType.EXIT)
	public void screenExit(ScreenEvent event, ScreenControl screenControl, DbContext ctx, PackingSlipEditor head) throws EventException {
		//ctx.out().print("is posted ? exit screen event" + head.getPostProcess());
	}

	@ScreenEventHandler(type = ScreenEventType.END)
	public void screenEnd(ScreenEndEvent event, ScreenControl screenControl, DbContext ctx, PackingSlipEditor head) throws EventException {
		//ctx.out().print("is posted ? end of ecreen" + head.getPostProcess());
		if(!head.getYekplsunderpono().isEmpty()){
			Iterable<Row> rows = head.table().getEditableRows();
			for(PackingSlipEditor.Row row : rows){
				if(!row.getYekplspoitemid().isEmpty()){
					//ctx.out().print(row.getYekplspoitemid());
					Item item = ctx.load(Item.class, new IdImpl(row.getYekplspoitemid()));
					
					PurchaseOrder  purchaseOrder = ctx.load(PurchaseOrder.class, new IdImpl(item.getHead().getId().toString()));
					PurchaseOrderEditor purchaseOrderEditor = purchaseOrder.createEditor();
					try{
						purchaseOrderEditor.open(EditorAction.UPDATE);
						Iterable<PurchaseOrderEditor.Row> purchaseOrderRows = purchaseOrderEditor.table().getEditableRows();
						for(PurchaseOrderEditor.Row purchaseOrderRow : purchaseOrderRows){
							if(purchaseOrderRow.getId().toString().equals(row.getYekplspoitemid())){
								purchaseOrderRow.setStatus("Z");
							}else{
								//ctx.out().print(" ----ID doesnt matched");
							}	
						}
						purchaseOrderEditor.commit();
						//ctx.out().print("ZZZP OPOP ");
					}catch(CommandException e){
						ctx.out().print(e.getMessage());
					}finally{
						
					}
					
				}
			}
		}		
	}
}
