package de.abas.p81.ekpls;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailHandler {
	
	  protected static  String senderEmail;
	  protected static  String senderPassword;
	  protected static String host;
	  protected static int port;
	  protected static boolean hasAuth;
	  protected static boolean enabletls;
	

	  public static void sendAsHtml(String to, String title, String html) throws Exception {
	      Session session = createSession();
	      MimeMessage message = new MimeMessage(session);
	      prepareEmailMessage(message, to, title, html);
	      Transport.send(message);
	  }

	  private static void prepareEmailMessage(MimeMessage message, String to, String title, String html)
	          throws MessagingException {
	      message.setContent(html, "text/html; charset=utf-8");
	      message.setFrom(new InternetAddress(senderEmail));
	      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
	      message.setSubject(title);
	  }

	  private static Session createSession() {
	      Properties props = new Properties();
	      props.put("mail.smtp.auth", hasAuth);
	      props.put("mail.smtp.starttls.enable", enabletls);
	      props.put("mail.smtp.host", host); 
	      props.put("mail.smtp.port", port);

	      Session session = Session.getInstance(props, new javax.mail.Authenticator() {
	          protected PasswordAuthentication getPasswordAuthentication() {
	              return new PasswordAuthentication(senderEmail, senderPassword);
	          }
	      });
	      return session;
	  }

}
