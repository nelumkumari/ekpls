package de.abas.p81.ekpls;

import de.abas.erp.api.gui.TextBox;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.vendor.VendorEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;

@EventHandler(head = VendorEditor.class)

@RunFopWith(EventHandlerRunner.class)

public class VendorEventHandler {

	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, VendorEditor head) throws EventException {
		new TextBox(ctx, "sdfsdf", "dsfsdf").show();
	}

}
